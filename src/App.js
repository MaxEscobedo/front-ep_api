import React from 'react';
import {Route,BrowserRouter,Switch,Redirect} from 'react-router-dom'
import Materias from './pages/Materias'
import Profesores from './pages/Profesores'
import Coments from './pages/Coments'
import Login from './pages/Login'
import Register from './pages/Register'
import Navbar from './components/Navbar'
import 'bootstrap/dist/css/bootstrap.min.css';
import Profile from './pages/Profile'
import Enlaces from './pages/Enlaces'
function App() {
  return (
    <div className="App app-body">
      <BrowserRouter>
        <Switch>
          <Route exact path={'/login'} component={Login}/>
          <Route exact path={'/register'} component={Register}/>
          <Route exact path={'/Materias'} component={Materias} />
          <Route exact path={'/Materias/Profesores/:idMateria'} component={Profesores}/>
          <Route exact path={'/Materias/Profesores/coments/:idTeacher/:idSubject'} component={Coments}/>
          <Route exact path={'/Perfil'} component={Profile}/>
          <Route exact path={'/Enlaces'} component={Enlaces}/>
          <Route exact path={'/'} component={Materias} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
