import React from 'react'
import {Link} from 'react-router-dom'
import './style.css'
const Cards =(props)=>{
    return(
      <React.Fragment>

      { [0,1,2,3,4,5].map((id)=>(
            <div key={id}>
              <div className="card topCard" >
                <div className="card-body">
                   <h4 className="card-title">Card title</h4>
                   <p className="card-text">Materia de Divec</p>
                  <Link to={`${props.route}${id}`} className="card-link">Ver Materia</Link>
               </div>
             </div>
             <br/>       
           </div>
            
    ))}
      
      </React.Fragment>
      
      
    )
}

export default Cards