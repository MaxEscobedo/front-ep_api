import React from 'react'
import {Link} from 'react-router-dom'
import './style.css'
import {Dropdown, DropdownButton} from 'react-bootstrap'
import {getType, getName, logout} from  '../../services/auth'
import {Redirect} from 'react-router-dom'

class NavbarBoots extends React.Component{
   state={
       type:getType(),
       active:true
   }
   
   
    handleLogout =()=>{
        this.setState({active:false})
        logout();
        this.setState({type:'0',active:true})
    }


    render(){
   
     if(this.state.type== '1'){
     
         return(
             <React.Fragment>
            <nav className="navbar navbar-light navbar-expand-md fixed-top color-nav"  >
                     <Link className="nav-link navbar-brand" to='/Materias' >Incio</Link>
                     
                             <DropdownButton className='nav-link'  id="dropdown-variants-Info" variant="info" title={getName()}>
                             <Dropdown.Item onClick={this.handleLogout} eventKey="1">Cerrar Sesion</Dropdown.Item>
                             </DropdownButton>
 
                     {!this.props.go && <input type="text" 
                                 className="form-control " 
                                 placeholder="Buscar..." 
                                 name="name"
                                 onKeyDown={this.props.handleOnChange}
                                 value={this.props.valueForm}
                                 />}
                 </nav>
             </React.Fragment>
         )
         
     }
 
     if(this.state.type== '2'){
     
         return(
             <React.Fragment>
            <nav className="navbar navbar-light navbar-expand-md fixed-top color-nav"  >
                     <Link className="nav-link navbar-brand" to='/Materias' >Incio</Link>
                     
                             <DropdownButton className='nav-link'  id="dropdown-variants-Info" title={getName()}>
                             <Dropdown.Item eventKey="1" ><Link to={'/Perfil'}>Perfil</Link></Dropdown.Item>
                             <Dropdown.Item  eventKey="2"><Link to={'/Enlaces'}>Enlazar Materias</Link></Dropdown.Item>
                             <Dropdown.Item onClick={this.handleLogout} eventKey="3">Cerrar Sesion</Dropdown.Item>
                             </DropdownButton>
 
                     {!this.props.go && <input type="text" 
                                 className="form-control " 
                                 placeholder="Buscar..." 
                                 name="name"
                                 onKeyDown={this.props.handleOnChange}
                                 value={this.props.valueForm}
                                 />}
                 </nav>
             </React.Fragment>
         )
         
     }
 
     if(this.state.type== '3'){
     
         return(
             <React.Fragment>
            <nav className="navbar navbar-light navbar-expand-md fixed-top color-nav"  >
                     <Link className="nav-link navbar-brand" to='/Materias' >Incio</Link>
                     
                             <DropdownButton className='nav-link'  id="dropdown-variants-Info" title={getName()}>
                             <Dropdown.Item  eventKey="1"><Link to={'/'}>Agregar Materias</Link></Dropdown.Item>
                             <Dropdown.Item  eventKey="2"><Link to={'/Enlaces'}>Enlazar Materias</Link></Dropdown.Item>
                             <Dropdown.Item onClick={this.handleLogout} eventKey="3">Cerrar Sesion</Dropdown.Item>
                             </DropdownButton>
 
                     {!this.props.go && <input type="text" 
                                 className="form-control " 
                                 placeholder="Buscar..." 
                                 name="name"
                                 onKeyDown={this.props.handleOnChange}
                                 value={this.props.valueForm}
                                 />}
                 </nav>
             </React.Fragment>
         )
         
     }
 
     return(
         <React.Fragment>
        <nav className="navbar navbar-light navbar-expand-md fixed-top color-nav"  >
                 <Link className="nav-link navbar-brand" to='/Materias' >Incio</Link>
                 
         
                         <Link className="nav-link" to='/login'>Login</Link>
     
                     
                         <Link className="nav-link" to='/register'>Sign</Link>
                 {!this.props.go && <input type="text" 
                             className="form-control " 
                             placeholder="Buscar..." 
                             name="name"
                             onKeyDown={this.props.handleOnChange}
                             value={this.props.valueForm}
                             />}
             </nav>
         </React.Fragment>
     )
 }
}

export default NavbarBoots