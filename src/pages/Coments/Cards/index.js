import React from 'react'
import {Link} from 'react-router-dom'
import './style.css'
const Cards =(props)=>{
    return(
      <React.Fragment>

      { props.comments.map((date,index)=>(
            <div key={index}>
              <div className="card topCard" >
                <div className="card-body">
                  <h4 className="card-title">{date.userName}</h4>
                  <p className="card-text">{date.comment}</p>
                
               </div>
             </div>
             <br/>       
           </div>
            
    ))}
      
      </React.Fragment>
      
      
    )
}

export default Cards