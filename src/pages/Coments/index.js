import React from 'react'
import Cards from './Cards'
import './style.css'
import NavbarBoots from '../../components/NavbarBoots'
import { getId } from "../../services/auth";
import {getComments,setComments, setCommentsLike} from '../../services/api/comment'
class Coments extends React.Component{
   state ={
       refresh:false,
       comments:[],
       comment:'',
       form:{
           idUser:parseInt(getId()),
           idTeacher:this.props.match.params.idTeacher,
           idSubject:this.props.match.params.idSubject,
       },
       dates:{
           teacher:{
               name:''
           },
           subject:{
               name: ''
           },
           subjects:[]
       }
   }
   handleChange = e => {
    this.setState({
        ...this.state,
        [e.target.name]: e.target.value,
      
    });
  };
   response = async()=>{
          
        const data = await getComments(this.state.form);
        console.log(data)
        if(data.success)
        this.setState({dates:data.dates})
        console.log(`state ${this.state.dates}`)
   }
    componentDidMount(){
       this.response()
   }
   handleSubmit= async ()=>{
       const form= {
            id_subject:this.state.form.idSubject,
            id_teacher:this.state.form.idTeacher,
            id_user:this.state.form.idUser,
            comment:this.state.comment
       }
        this.setState({refresh:true})
        const responses = await setComments(form)
        if(responses.success){
            this.response();
        }
        this.setState({refresh:false,comment:""})

   }
   positiveSubmit= async ()=>{
    const form= {
        idSubject:this.state.form.idSubject,
        idTeacher:this.state.form.idTeacher,
        idUser:this.state.form.idUser,
        negative_vote: 0,
        positive_vote: 1
      }
      const responses = await setCommentsLike(form)
      console.log(responses)
   }
    negativeSubmit= async ()=>{
        const form= {
            idSubject:this.state.form.idSubject,
            idTeacher:this.state.form.idTeacher,
            idUser:this.state.form.idUser,
            negative_vote: 1,
            positive_vote: 0
          }
          const responses = await setCommentsLike(form)
          console.log(responses)
       }
   
    render(){

        return(
            <React.Fragment >
                <NavbarBoots go={true}/>
                <br/>
                <div className=' mt-5'>
                <h1 className='text-center'>Profesor</h1>
                <div className='row'>
                    <div className='col-1'></div>
                    <div className='col'>
                    <div className="card">
                    <div className="card-header text-center">
                        {this.state.dates.teacher.name}
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">{this.state.dates.subject.name}</h5>
                        <p className="card-text">Informacion del  profesor.</p>
                        <p className="card-text"><b>Materias impartidas por el profesor:</b></p>
                        {this.state.dates.subjects.map((subject)=>(
                            <div key={subject.id}>
                            <p className="card-text">{subject.name}</p>
                            </div>
                        ))}
                        {getId()&& 
                        <div>
                                    <button className="btn btn-primary mg-2" onClick={this.positiveSubmit}>Like</button>
                                    <button className="btn btn-danger mg-5"  onClick={this.negativeSubmit}>disLike</button>
                        </div>}
                    </div>
                </div>
                
                        {getId() && <div className="form-group">
                            <label for="comment">Comentarios:</label>
                             <textarea className="form-control" rows="5"  
                             onChange={this.handleChange}
                             value={this.state.comment}
                             name="comment" 
                             id="comment"></textarea>
                            <button className='btn btn-success' onClick={this.handleSubmit}>Enviar</button>
                        </div>}

                    </div>
                    <div className='col-4'>
                    <div className="scroll-div">
                        { this.state.dates.comments &&<Cards  comments={this.state.dates.comments}/>}
                    </div>
                    </div>
                </div>
               </div>
            </React.Fragment>
        )
    }
}

export default Coments