import React from 'react'
import {Redirect} from 'react-router-dom'
import "./images/icons/favicon.ico"
import "./fonts/font-awesome-4.7.0/css/font-awesome.min.css"
import "./fonts/Linearicons-Free-v1.0.0/icon-font.min.css"
import "./vendor/animate/animate.css"
import "./vendor/css-hamburgers/hamburgers.min.css"
import "./vendor/animsition/css/animsition.min.css"
import "./vendor/select2/select2.min.css"	
import "./vendor/daterangepicker/daterangepicker.css"
import "./css/util.css"
import "./css/main.css"
import {getType, getId} from '../../services/auth'
import NavbarBoots from '../../components/NavbarBoots'

class Enlaces extends React.Component{
	state ={
		form:{
			idUser:'',
        	idMateria:'',
        	
		},
		redirect:false,
		getype:getType(),
	}

	handleChange = e => {
		this.setState({
		  form: {
			...this.state.form,
			[e.target.name]: e.target.value,
		  },
		});
	  };

	handleSubmit = ()=>{
		
		this.setState({redirect:true})
	}

    render(){
        return(
            <React.Fragment>
				{this.state.redirect && <Redirect to='/Materias' />}
				<NavbarBoots go={true} />
                	<div className="limiter">
		<div className="container-login100">
			<div className="wrap-login100">
				<div className="login100-form-title" >
					<span className="login100-form-title-1">
						Enlazar Materias
					</span>
				</div>

				<form	 className="login100-form validate-form">
					  <div className="wrap-input100 validate-input m-b-26" data-validate="email is required">
						<span className="label-input100">Id Usuario</span>
						<input className="input100" 
						type="text" 
						name="idUser" 
						placeholder="Enter IdUser"
						onChange={this.handleChange}
						value={this.state.form.idUser}
						/>
						<span className="focus-input100"></span>
					</div>

					<div className="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span className="label-input100">Id Materia</span>
						<input className="input100" 
						type="password" 
						name="idMateria" 
						placeholder="Enter Id Materia"
						onChange={this.handleChange}
						value={this.state.form.idMateria}
						/>
						<span className="focus-input100"></span>
					</div>

					<div className="container-login100-form-btn">
						<button type='button' onClick={this.handleSubmit} className="login100-form-btn">
							Enlazar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>   
            </React.Fragment>
        )
    }
}

export default Enlaces