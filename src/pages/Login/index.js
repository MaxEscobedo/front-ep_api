import React from 'react'
import {Link,Redirect} from 'react-router-dom'
import "./images/icons/favicon.ico"
import "./fonts/font-awesome-4.7.0/css/font-awesome.min.css"
import "./fonts/Linearicons-Free-v1.0.0/icon-font.min.css"
import "./vendor/animate/animate.css"
import "./vendor/css-hamburgers/hamburgers.min.css"
import "./vendor/animsition/css/animsition.min.css"
import "./vendor/select2/select2.min.css"	
import "./vendor/daterangepicker/daterangepicker.css"
import "./css/util.css"
import "./css/main.css"
import {login} from '../../services/auth'
import {LoginUser} from '../../services/api/auth'
import NavbarBoost from '../../components/NavbarBoots'
class Login extends React.Component{
	state ={
		form:{
			email:'',
        	password:'',
        	
		},
		redirect:false
	}

	handleChange = e => {
		this.setState({
		  form: {
			...this.state.form,
			[e.target.name]: e.target.value,
		  },
		});
	  };

	handleSubmit = async()=>{
		const response = await LoginUser(this.state.form)
		console.log(response)
			 if(response.success){
				login(response.user.name,String(response.user.type),response.user.id)
		   		this.setState({redirect:true})
			 }
			
	}

    render(){
        return(
            <React.Fragment>
				{this.state.redirect && <Redirect to='/Materias' />}
				<NavbarBoost go={true}/>
                	<div className="limiter">
		<div className="container-login100">
			<div className="wrap-login100">
				<div className="login100-form-title" >
					<span className="login100-form-title-1">
						Iniciar Sesion
					</span>
				</div>

				<form	 className="login100-form validate-form">
					<div className="wrap-input100 validate-input m-b-26" data-validate="email is required">
						<span className="label-input100">Email</span>
						<input className="input100" 
						type="email" 
						name="email" 
						placeholder="Enter email"
						onChange={this.handleChange}
						value={this.state.form.email}
						/>
						<span className="focus-input100"></span>
					</div>

					<div className="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span className="label-input100">Password</span>
						<input className="input100" 
						type="password" 
						name="password" 
						placeholder="Enter password"
						onChange={this.handleChange}
						value={this.state.form.password}
						/>
						<span className="focus-input100"></span>
					</div>

					<div className="flex-sb-m w-full p-b-30">

						<div>
							<Link to={'/register'} className="txt1">
								Registrate
							</Link>
						</div>
					</div>

					<div className="container-login100-form-btn">
						<button type='button' onClick={this.handleSubmit} className="login100-form-btn">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>   
            </React.Fragment>
        )
    }
}

export default Login