import React from 'react'
import {Link} from 'react-router-dom'
import './style.css'
const Cards =(props)=>{
    return(
      <React.Fragment>

      { props.data.map((materia)=>(
            <div key={materia.id}>
              <Link to={`${props.route}${materia.id}`}>
              <div className="card topCard" >
                <div className="card-body">
                  <h4 className="card-title">{materia.name}</h4>
                  <p className="card-text">codigo de materia: {materia.key_subject} <br/> creditos: {materia.credits}</p>
                  Ver Materia
               </div>
             </div>
               </Link>
             <br/>       
           </div>
            
    ))}
      
      </React.Fragment>
      
      
    )
}

export default Cards