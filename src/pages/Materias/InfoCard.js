import React from 'react'


const InfoCard = (props)=>
{
    return(
        <React.Fragment>
           
              <div className="card topCard" >
                <div className="card-body">
                   <h4 className="card-title">Materias Divec</h4>
                   <p className="card-text">En esta pagina podras encontrar las reseñas
                    mas importantes de los profesores de el area de computacion, podras ver 
                    por materia como se desemeña cada profesor, asi como tambian observar el
                    puntaje promedio que los usuarios le ponemos a los profesores.</p>
                  <a href="http://www.cucei.udg.mx/carreras/informatica/es/malla-curricular-inni" target="_blank" className="card-link">Ver malla INNI</a>
                  <a href="http://www.cucei.udg.mx/carreras/computacion/?q=contenido/malla-curricular-inco" target="_blank" className="card-link">Ver malla INCO</a>

                  <h4 className="card-title">Mapa de cucei</h4>
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3733.33657504745!2d-103.3245261123986!3d20.655881500460875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8428b23a9bbba80d%3A0xdacdb7fd592feb90!2sCentro%20Universitario%20de%20Ciencias%20Exactas%20e%20Ingenier%C3%ADas!5e0!3m2!1ses-419!2smx!4v1574116153352!5m2!1ses-419!2smx" width="100%" height="450" frameborder="0" style={{border: 0, margin:5}} allowfullscreen=""></iframe>

               </div>
             </div>
             <br/>       
           
        </React.Fragment>
    )
}


export default InfoCard;