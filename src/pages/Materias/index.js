import React from 'react'
import Navbar from '../../components/Navbar'
import Cards from './Cards'
import NavbarBoots from '../../components/NavbarBoots'
import InfoCard from './InfoCard'
import {getMaterias} from '../../services/api/materias'
class Materias extends React.Component{

    state={ 
        form:{
            name:'',
            id:''
        },
        subjects:[
            {
                id:'',
                name:'',
                credits:'',
                key_subject:'',
            }
        ]
    }
    reponse =async()=>{
        const responseData = await getMaterias(this.state.form);
        this.setState({subjects:responseData.subjects})
    }
    componentDidMount ()
    {
        this.reponse();
    }
    handleChange = async e => {
		this.setState({
		  form: {
			...this.state.form,
			[e.target.name]: e.target.value,
		  },
        });
        try{
            const responseData = await getMaterias(this.state.form);
            if(responseData.subjects.subjects)
            this.setState({subjects: responseData.subjects.subjects})
            else
            this.setState({subjects: responseData.subjects})
        }catch(error){
            console.log(error)
        }
        
	  }

    render(){
        return(
            <React.Fragment>
                <NavbarBoots handleOnChange={this.handleChange} valueForm={this.state.name}/>
                <br/>
                <br/>
                <br/>
                <h1 className='text-center'>Materias</h1>
                <div className="row">
                    <div className="col-4">
                    <InfoCard/>
                    </div>
                    <div className="col-7">
                        { this.state.subjects &&<Cards route={'/Materias/Profesores/'} data={this.state.subjects} />}
                    </div>
                </div>

            </React.Fragment>
        )
    }
}

export default Materias