import React from 'react'
import {Link} from 'react-router-dom'
import './style.css'
import {Chart} from 'react-google-charts'
const Cards =(props)=>{
    return(
      <React.Fragment>

      { props.data.map((teacher)=>{
        const votes =teacher.positive_vote+teacher.negative_vote;
        return(
            <div key={teacher.id}>
              <Link to={`${props.route}${teacher.id}/${props.idSubject}`}>
              <div className="card topCard" >
                <div className="card-body">
                  <h4 className="card-title">{teacher.name}</h4>

                {votes!==0 && 
                 <Chart
                 width={'250px'}
                 height={'150px'}
                 chartType="PieChart"
                 loader={<div>Loading Chart</div>}
                  data={[
                  ['Votos', 'Cantidad'],
                  ['Positivos', teacher.positive_vote],
                  ['Negativos', teacher.negative_vote],
                  
                  ]}
                  options={{
                    title: 'Promedio de Votos:',
                  }}
                   rootProps={{ 'data-testid': '1' }}
                 />}
                 


                  Ver Comentarios
               </div>
             </div>
             <br/>       
             </Link>
           </div>
            
    )
  }
    )}
      
      </React.Fragment>
      
      
    )
}

export default Cards