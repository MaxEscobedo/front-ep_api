import React from 'react'


const InfoCard = (props)=>
{
    return(
        <React.Fragment>
           
           { props.data.map((materia)=>(
            <div key={materia.id}>
              <div className="card topCard" >
                <div className="card-body">
                  <h4 className="card-title">{materia.name}</h4>
                  <p className="card-text">codigo de materia: {materia.key_subject} <br/> creditos: {materia.credits}</p>
               </div>
             </div>
             <br/>       
           </div>
            
    ))}
             <br/>       
           
        </React.Fragment>
    )
}


export default InfoCard;