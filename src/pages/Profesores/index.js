import React from 'react'
import Navbar from '../../components/Navbar'
import Cards from './Cards'
import NavbarBoots from '../../components/NavbarBoots'
import {getTeachersGet} from '../../services/api/teacher'
import InfoCard from './InfoCard'
class Profesores extends React.Component{

    state={ 
        form:{
            name:'',
            id:this.props.match.params.idMateria
        },
        subjects:{
            teachers:[],
            subject:[]
        },
    }
    resposnseData = async ()=>{
        const response = await getTeachersGet(this.state.form)
        console.log(response);
        this.setState({subjects:response.subjects})
        console.log(this.state.subjects)
    }
    componentDidMount(){
        this.resposnseData()
    }
    handleChange = async e => {
		this.setState({
		  form: {
			...this.state.form,
			[e.target.name]: e.target.value,
		  },
        });
        const response = await getTeachersGet(this.state.form)
        this.setState({subjects:response.subjects})
       
	  }

    render(){
        return(
            <React.Fragment>
                  <NavbarBoots handleOnChange={this.handleChange} valueForm={this.state.name}/>
                <br/>
                <br/>
                <br/>
                <h1 className='text-center'>Profesores</h1>
                <div className="row">
                    <div className="col-4">
                   <InfoCard data={this.state.subjects.subject}/>
                    </div>
                    <div className="col-7">
                        {this.state.subjects.teachers &&<Cards route={'/Materias/Profesores/coments/'} data={this.state.subjects.teachers} idSubject={this.state.form.id}/>}
                    </div>
                </div>

            </React.Fragment>
        )
    }
}

export default Profesores