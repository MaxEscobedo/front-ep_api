import React from 'react'
import NavbarBoots from '../../components/NavbarBoots'
class Profile extends React.Component {
    render(){
        return(
            <React.Fragment>
                <NavbarBoots go={true}/>
                 <br/>
                
                <h1 className='text-center'>Profesor</h1>
                <div className='row'>
                    <div className='col-1'></div>
                    <div className='col'>
                    <div className="card">
                    <div className="card-header text-center">
                        David Anaya
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">Algoritmia</h5>
                        <p className="card-text">Informacion del  profesor.</p>
                        <a href="#" className="btn btn-primary mg-2">Like</a>
                        <a href="#" className="btn btn-danger mg-5">disLike</a>
                    </div>
                </div>
                </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Profile;