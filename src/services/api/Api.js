const urlBase="http://187.144.26.101/ep_api/public/api"

export const Api= async (route,data)=>{
    const url=urlBase+route;
    try{
        const response = await fetch(url,data);
        
        const data1 = await response.json();
        return data1;
    }
    catch(error){
        console.log(error)
    }
    
}