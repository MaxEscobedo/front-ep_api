import {Api} from './Api'
import { login } from '../auth'

export const LoginUser =(data)=>{
   return Api('/login',{
        headers:{
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        method:'POST',
        body: JSON.stringify(data),
    })

}

export const RegisterUser =(data)=>{
    return Api('/register',{
        headers:{
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        method:'POST',
        body:JSON.stringify(data)
    })
 
 }