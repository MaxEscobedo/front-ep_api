import {Api} from './Api'


export const setComments =(data)=>{
  console.log(data)
   return Api(`/comment`,{
        headers:{
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        method:'POST',
        body: JSON.stringify(data)
    })

}
export const setCommentsLike =(data)=>{
  console.log(data)
   return Api(`/commentLike`,{
        headers:{
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        method:'POST',
        body: JSON.stringify(data)
    })

}


export const getComments =(data)=>{
  console.log(data)
    return Api(`/comment?idSubject=${data.idSubject}&idTeacher=${data.idTeacher}`,{
         headers:{
             'Content-Type': 'application/json',
             Accept: 'application/json',
           },
         method:'GET'
     })
 
 }