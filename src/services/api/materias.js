import {Api} from './Api'


export const getMaterias =(data)=>{
   return Api(`/subject?name=${data.name}&id=${data.id}`,{
        headers:{
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        method:'GET'
    })

}

export const getMateriasGet =(data)=>{
    return Api('/subject',{
         headers:{
             'Content-Type': 'application/json',
             Accept: 'application/json',
           },
         method:'GET',
         
     })
 
 }