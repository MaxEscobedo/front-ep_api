import {Api} from './Api'
import { login } from '../auth'

export const getTeachers =(data)=>{
   return Api('/subjects',{
        headers:{
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        method:'POST',
        body: JSON.stringify(data),
    })

}

export const getTeachersGet =(data)=>{
    return Api(`/subject?id=${data.id}&name=${data.name}`,{
         headers:{
             'Content-Type': 'application/json',
             Accept: 'application/json',
           },
         method:'GET',
         
     })
 
 }