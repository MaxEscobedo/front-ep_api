export const login= (name,type,id)=>{
    localStorage.setItem('name',name);
    localStorage.setItem('type',type);
    localStorage.setItem('id',id);
}

export const getName= ()=>(
    localStorage.getItem('name')
)

export const getType= ()=>(
    localStorage.getItem('type')
)

export const getId= ()=>(
    localStorage.getItem('id')
)

export const logout= ()=>{
    localStorage.removeItem('name');
    localStorage.removeItem('type');
    localStorage.removeItem('id')
}